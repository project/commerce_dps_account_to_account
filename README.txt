Commerce DPS Account 2 Account
================

Introduction
------------

Commerce DPS Account 2 Account is a [Drupal
Commerce](https://drupal.org/project/commerce) module that integrates the 
[Account 2 Account]
(http://www.paymentexpress.com/Products/Ecommerce/account2account)
payment gateway into your Drupal
Commerce shop.


Features
--------

1.  No SSL certificate requried

2.  Fail-proof result notification


Basic Installation
------------------

1.  Download and enable the module.

4.  Configure the payment rule (the `edit` link) with your keys.

5.  Done.
